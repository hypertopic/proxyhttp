ProxyHTTP
=========

A HTTP proxy with basic authentication and authorization management

Description
-------------------------

Simple HTTP proxy in node.js

The proxy is divide into two file:

* ProxyHTTP.js which provide the basic functions of the proxy

* config.json in which the use define the configuration for its proxy

Program requirement 
----------------------

* Install node.js and the package ldapjs (npm install)

* Create a config.json without comments or config.js with comments

* Modify the port number inside ProxyHTTP.js 

* Start ProxyHTTP.js  ( # node ProxyHTTP.js)
